import click
import structlog

from .. import logging

@click.command()
@click.option('--epochs', default=1, help='Number of epochs.')
def main(epochs):
    logger = structlog.get_logger('bob')

    for epoch in range(1, epochs+1):
        logger.info("epoch", epoch=epoch)

if __name__ == '__main__':
    main()
