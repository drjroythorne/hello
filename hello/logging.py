import sys
import structlog
import logging
from logging import handlers
from pythonjsonlogger import jsonlogger

# Setup jsonlogger to print JSON
json_handler = handlers.RotatingFileHandler('/var/log/hello/hello.log')
json_handler.setFormatter(jsonlogger.JsonFormatter())

# Add both handler to logging
logging.basicConfig(
    format="%(message)s",
    handlers=[json_handler],
    level=logging.INFO
)

structlog.configure(
    processors=[
        structlog.stdlib.filter_by_level,
        structlog.stdlib.add_logger_name,
        structlog.stdlib.add_log_level,
        structlog.stdlib.PositionalArgumentsFormatter(),
        structlog.processors.StackInfoRenderer(),
        structlog.processors.format_exc_info,
        structlog.processors.UnicodeDecoder(),
        structlog.stdlib.render_to_log_kwargs,
    ],
    context_class=dict,
    logger_factory=structlog.stdlib.LoggerFactory(),
    wrapper_class=structlog.stdlib.BoundLogger,
    cache_logger_on_first_use=True
)


