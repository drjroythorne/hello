To install within a virtualenvwrapper virtual environment with dev and test dependencies.

pipx run flit install --symlink --env --python $VIRTUAL_ENV/bin/python --extras test,dev


Get logging to work with filebeat, Elastic, Kibana. For now, just do the following in separate shells.

1. Run kibana (just downloaded 64bit binary from https://www.elastic.co/downloads/kibana)

    ~/bin/kibana-7.2.0-linux-x86_64/bin/kibana

2. Run elastic (using Docker for now):

    docker run --rm -d -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:7.3.0

3. First configure filebeat (add filebeat.yml to /etc/filebeat first)

    filebeat setup

Then run it:

    filebeat run &
    
    



